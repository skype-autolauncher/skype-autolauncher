/** @skype-autolauncher-home.c
 * \brief Hildon Desktop item for Automatic Skype Launcher
 *
 * This item let's the user to launch skype directly from the Hildon
 * Destkop.
 *
 * Copyright (C) 2011, Igalia, S.L.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Andres Gomez <agomez@igalia.com>
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>

#include <glib.h>
#include <gtk/gtk.h>

#include <libosso.h>

#include <hildon/hildon-banner.h>

#include <libhildondesktop/libhildondesktop.h>

#include "skype-autolauncher-home.h"


HD_DEFINE_PLUGIN (SkypeAutolauncherHomeItem, skype_autolauncher_home_item, HILDON_DESKTOP_TYPE_HOME_ITEM);

typedef struct _SkypeAutolauncherHomeItemPrivate SkypeAutolauncherHomeItemPrivate;
struct _SkypeAutolauncherHomeItemPrivate
{
    osso_context_t *osso;
};
#define SKYPE_AUTOLAUNCHER_HOME_ITEM_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE((o), \
				     SKYPE_AUTOLAUNCHER_TYPE_HOME_ITEM, \
				     SkypeAutolauncherHomeItemPrivate))

static const gchar *skype_service = "com.nokia.skype";
/* static const gchar *skype_path = "/"; */
/* static const gchar *skype_interface = "com.nokia.skype"; */
static const gchar *skype_autolauncher_rc = ""
"pixmap_path \"/usr/share/themes/default/gtk-2.0:/usr/share/pixmaps\""
""
"style \"skype-autolauncher-applet\""
"{"
"  xthickness = 0"
"  ythickness = 0"
"  HildonDesktopHomeItem::background-borders = { 0, 0, 0, 0 }"
"  bg_pixmap[PRELIGHT] = \"skype-autolauncher/skype_autolauncher_home_bg_alpha.png\""
""
"  engine \"sapwood\""
"    {"
"      image"
"        {"
"          function = BOX"
"          file     = \"skype-autolauncher/skype_autolauncher_home_bg.png\""
"          shaped = TRUE"
"        }"
"      image"
"        {"
"          function = SHADOW"
"          file     = \"skype-autolauncher/skype_autolauncher_home_bg.png\""
"          shaped = TRUE"
"        }"
"    }"
"}"
"widget \"*.skype-autolauncher-applet\" style \"skype-autolauncher-applet\""
""
"style \"skype-autolauncher-button\""
"{"
"  GtkButton::default_border = { 0, 0, 0, 0 }"
"  GtkButton::default_outside_border = { 0, 0, 0, 0 }"
"  GtkButton::child_spacing = 0"
"  xthickness = 0"
"  ythickness = 0"
""
""
"  engine \"sapwood\" {"
"    image {"
"      function = BOX"
"      state = ACTIVE"
"    }"
"    image {"
"      function = FOCUS"
"    }"
"    image {"
"      function = BOX"
"    }"
"  }"
""
"}"
"widget \"*.skype-autolauncher-button\" style \"skype-autolauncher-button\"";


static GtkWidget * _skype_autolauncher_button_new (void);
static GtkWidget * _skype_autolauncher_inner_widget_new (void);
static void _apply_styling (GtkWidget *home_item);
static gboolean _applet_expose (GtkWidget *widget, GdkEventExpose *event);
static void _launch_skype (GtkButton *button, osso_context_t *osso);


/* --------------------------------------------------------------------------- */


static void
skype_autolauncher_home_item_init (SkypeAutolauncherHomeItem *home_item)
{
    GtkWidget *button;
    GtkWidget *inner_widget;
    SkypeAutolauncherHomeItemPrivate *priv = NULL;


    priv = SKYPE_AUTOLAUNCHER_HOME_ITEM_GET_PRIVATE (home_item);

    priv->osso = osso_initialize("skype_autolauncher",
                                 "0.0.2",
                                 FALSE,
                                 NULL);

    button = _skype_autolauncher_button_new ();
    inner_widget = _skype_autolauncher_inner_widget_new ();

    g_signal_connect (button, "clicked",
                      G_CALLBACK (_launch_skype),
                      priv->osso);
    g_signal_connect ((gpointer) inner_widget, "expose-event",
                      G_CALLBACK (_applet_expose), NULL);

    gtk_container_add (GTK_CONTAINER (inner_widget), button);
    gtk_container_add (GTK_CONTAINER (home_item), inner_widget);

    gtk_widget_show_all (inner_widget);

    _apply_styling (GTK_WIDGET (home_item));
}

static void
skype_autolauncher_home_item_finalize (GObject *object)
{
    SkypeAutolauncherHomeItemPrivate *priv = NULL;


    priv = SKYPE_AUTOLAUNCHER_HOME_ITEM_GET_PRIVATE (object);

    osso_deinitialize (priv->osso);

    G_OBJECT_CLASS (g_type_class_peek_parent (G_OBJECT_GET_CLASS (object)))->finalize (object);
}

static void
skype_autolauncher_home_item_class_init (SkypeAutolauncherHomeItemClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    object_class->finalize = skype_autolauncher_home_item_finalize;
    g_type_class_add_private (klass, sizeof (SkypeAutolauncherHomeItemPrivate));
}


/* --------------------------------------------------------------------------- */

static GtkWidget *
_skype_autolauncher_button_new (void)
{
    GtkIconTheme *icon_theme;
    GdkPixbuf *icon;
    GtkWidget *icon_image, *button;


    icon_theme = gtk_icon_theme_get_default ();
    icon = gtk_icon_theme_load_icon (icon_theme,
                                     "skype",
                                     64,
                                     0,
                                     NULL);

    if (NULL == icon) {
        icon = gtk_icon_theme_load_icon (icon_theme,
                                         "skype-autolauncher",
                                         64,
                                         0,
                                         NULL);
    }

    if (NULL == icon) {
        icon = gtk_icon_theme_load_icon (icon_theme,
                                         "qgn_list_gene_default_app",
                                         64,
                                         0,
                                         NULL);
    }

    icon_image = gtk_image_new_from_pixbuf (icon);
    g_object_unref (G_OBJECT (icon));
    button = g_object_new (GTK_TYPE_BUTTON,
                           "name", "skype-autolauncher-button",
                           "visible", TRUE,
                           "can-focus", TRUE,
                           "can-default", TRUE,
                           "focus-on-click", TRUE,
                           NULL);
    gtk_button_set_alignment(GTK_BUTTON (button), 0.5, 0.5);
    gtk_button_set_relief(GTK_BUTTON (button), GTK_RELIEF_NONE);

    gtk_container_add  (GTK_CONTAINER (button), icon_image);

    return button;
}

static GtkWidget *
_skype_autolauncher_inner_widget_new (void)
{
    GtkWidget *applet_frame = NULL;


    applet_frame = g_object_new (GTK_TYPE_FRAME,
                                 "visible", TRUE,
                                 "name", "skype-autolauncher-applet",
                                 "can-focus", FALSE,
                                 NULL);

    return applet_frame;
}

static void
_apply_styling (GtkWidget *home_item)
{
    gtk_rc_parse_string (skype_autolauncher_rc);
    gtk_widget_set_name (home_item, "skype-autolauncher-applet");

    gtk_widget_set_size_request (home_item, 100, 100);
}

static gboolean
_applet_expose (GtkWidget *widget, GdkEventExpose *event)
{
  if (GTK_WIDGET_VISIBLE (widget))
    {
      gtk_paint_box (widget->style,
                     widget->window,
                     GTK_STATE_NORMAL,
                     GTK_SHADOW_NONE,
                     &event->area,
                     widget,
                     NULL,
                     widget->allocation.x,
                     widget->allocation.y,
                     widget->allocation.width,
                     widget->allocation.height);

      GTK_WIDGET_CLASS (g_type_class_peek_parent (GTK_FRAME_GET_CLASS (widget)))->
          expose_event (widget, event);
      return TRUE;
    }

  return FALSE;
}

static void
_launch_skype (GtkButton *button,
               osso_context_t *osso)
{
    hildon_banner_show_information  (GTK_WIDGET (button),
                                     NULL,
                                     "Launching Skype ...");

    /* osso_rpc_run (osso, */
    /*               skype_service, */
    /*               skype_path, */
    /*               skype_interface, */
    /*               NULL, */
    /*               NULL, */
    /*               DBUS_TYPE_INVALID); */

    osso_application_top (osso,
                          skype_service,
                          NULL);
}
