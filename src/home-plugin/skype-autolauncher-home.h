/** @skype-autolauncher-home.h
 * \brief Hildon Desktop item for Automatic Skype Launcher
 *
 * This item let's the user to launch skype directly from the Hildon
 * Destkop.
 *
 * Copyright (C) 2011, Igalia, S.L.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Andres Gomez <agomez@igalia.com>
 */


#ifndef SKYPE_AUTOLAUNCHER_HOME_H
#define SKYPE_AUTOLAUNCHER_HOME_H

#include <glib-object.h>

#include <libhildondesktop/libhildondesktop.h>

G_BEGIN_DECLS

/* Common struct types declarations */
typedef struct _SkypeAutolauncherHomeItem SkypeAutolauncherHomeItem;
typedef struct _SkypeAutolauncherHomeItemClass SkypeAutolauncherHomeItemClass;

/* Common macros */
#define SKYPE_AUTOLAUNCHER_TYPE_HOME_ITEM            (skype_autolauncher_home_item_get_type ())
#define SKYPE_AUTOLAUNCHER_HOME_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SKYPE_AUTOLAUNCHER_TYPE_HOME_ITEM, SkypeAutolauncherHomeItem))
#define SKYPE_AUTOLAUNCHER_HOME_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  SKYPE_AUTOLAUNCHER_TYPE_HOME_ITEM, SkypeAutolauncherHomeItemClass))
#define SKYPE_AUTOLAUNCHER_IS_HOME_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SKYPE_AUTOLAUNCHER_TYPE_HOME_ITEM))
#define SKYPE_AUTOLAUNCHER_IS_HOME_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  SKYPE_AUTOLAUNCHER_TYPE_HOME_ITEM))
#define SKYPE_AUTOLAUNCHER_HOME_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  SKYPE_AUTOLAUNCHER_TYPE_HOME_ITEM, SkypeAutolauncherHomeItemClass))

/* Instance struct */
struct _SkypeAutolauncherHomeItem
{
	HildonDesktopHomeItem hitem;
};

/* Class struct */
struct _SkypeAutolauncherHomeItemClass
{
	HildonDesktopHomeItemClass parent_class;
};

GType  skype_autolauncher_home_item_get_type  (void);
G_END_DECLS

#endif
