/** @skype-autolauncher-cp-applet.c
 * \brief Hildon Control Panel applet for Automatic Skype Launcher
 *
 * This applet let's the user select whether to launch Skype at boot
 * time and whether to monitor it's life through the dsmetool
 * watchdog.
 *
 * Copyright (C) 2011, Igalia, S.L.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Andres Gomez <agomez@igalia.com>
 */


#include <libosso.h>
#include <alarm_event.h>
#include <hildon-cp-plugin/hildon-cp-plugin-interface.h>
#include <hildon/hildon-time-editor.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include <string.h>
#include <time.h>


static const gchar *gconf_skype_autolauncher_path = "/system/skype-autolauncher";
static const gchar *gconf_fullscreen_active_key = "fullscreen_active";
static const gchar *gconf_init_active_key = "init_active";
static const gchar *gconf_dsme_active_key = "dsme_active";
static const gchar *gconf_alarm_event_key = "alarm_event";
static const gchar *gconf_alarm_reboot_key = "alarm_reboot";
static const alarm_event_t alarm_template = {0, 1440, -1, 0,
                                             NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                                             "/etc/cron.daily/skype-autolauncher",
                                             ALARM_EVENT_NO_DIALOG | ALARM_EVENT_BACK_RESCHEDULE,
                                             0};
static const gchar *init_script = "/etc/init.d/x-skype-autolauncher";

static void _get_gconf_settings (gboolean *fullscreen_active,
                                 gboolean *init_active,
                                 gboolean *dsme_active,
                                 gint *alarm_event,
                                 gboolean *alarm_reboot);
static void _set_gconf_settings (gboolean fullscreen_active,
                                 gboolean init_active,
                                 gboolean dsme_active,
                                 gint alarm_event,
                                 gboolean alarm_reboot);
static void _activate_associated_widget_to_toggle_toggled (GtkToggleButton *toggle_button,
                                                           GtkWidget *widget);
static gboolean _compare_alarm_events (alarm_event_t *first_event,
                                       alarm_event_t *second_event);
static void _set_alarm_widgets (gint *alarm_event,
                                GtkWidget **alarm_check,
                                GtkWidget **alarm_time_editor);
static gint _set_alarm (gint alarm_event,
                        gboolean alarm_active,
                        guint hours,
                        guint minutes);
static gboolean _restart_init (void);
static GtkDialog* _skype_autolauncher_dialog_new (GtkWidget **fullscreen_check,
                                                  GtkWidget **init_check,
                                                  GtkWidget **dsme_check,
                                                  GtkWidget **alarm_check,
                                                  GtkWidget **reboot_check,
                                                  GtkWidget **alarm_time_editor);
static void _skype_autolauncher_dialog_show ();


/* --------------------------------------------------------------------------- */


osso_return_t
execute (osso_context_t *osso, gpointer data,
	 gboolean user_activated)
{
    _skype_autolauncher_dialog_show ();
    return OSSO_OK;
}


/* --------------------------------------------------------------------------- */


static void
_get_gconf_settings (gboolean *fullscreen_active,
                     gboolean *init_active,
                     gboolean *dsme_active,
                     gint *alarm_event,
                     gboolean *alarm_reboot)
{
    GConfClient *client = NULL;
    GConfEntry *fullscreen_active_entry = NULL;
    GConfEntry *init_active_entry = NULL;
    GConfEntry *dsme_active_entry = NULL;
    GConfEntry *alarm_event_entry = NULL;
    GConfEntry *alarm_reboot_entry = NULL;
    GConfValue *fullscreen_active_value = NULL;
    GConfValue *init_active_value = NULL;
    GConfValue *dsme_active_value = NULL;
    GConfValue *alarm_event_value = NULL;
    GConfValue *alarm_reboot_value = NULL;
    gchar *tmp_string = NULL;


    g_assert (NULL != fullscreen_active);
    g_assert (NULL != init_active);
    g_assert (NULL != dsme_active);
    g_assert (NULL != alarm_event);
    g_assert (NULL != alarm_reboot);


    client = gconf_client_get_default ();

    if (!gconf_client_dir_exists (client,
                                  gconf_skype_autolauncher_path,
                                  NULL)) {
        goto cleanup;
    }

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_fullscreen_active_key);
    fullscreen_active_entry = gconf_client_get_entry (client,
                                                      tmp_string,
                                                      NULL,
                                                      TRUE,
                                                      NULL);
    g_free (tmp_string);

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_init_active_key);
    init_active_entry = gconf_client_get_entry (client,
                                                tmp_string,
                                                NULL,
                                                TRUE,
                                                NULL);
    g_free (tmp_string);

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_dsme_active_key);
    dsme_active_entry = gconf_client_get_entry (client,
                                                tmp_string,
                                                NULL,
                                                TRUE,
                                                NULL);
    g_free (tmp_string);

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_alarm_event_key);
    alarm_event_entry = gconf_client_get_entry (client,
                                                tmp_string,
                                                NULL,
                                                TRUE,
                                                NULL);
    g_free (tmp_string);

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_alarm_reboot_key);
    alarm_reboot_entry = gconf_client_get_entry (client,
                                                tmp_string,
                                                NULL,
                                                TRUE,
                                                NULL);
    g_free (tmp_string);

    fullscreen_active_value = gconf_entry_get_value (fullscreen_active_entry);

    if (NULL != fullscreen_active_value) {
        *fullscreen_active = gconf_value_get_bool (fullscreen_active_value);
    }

    init_active_value = gconf_entry_get_value (init_active_entry);

    if (NULL != init_active_value) {
        *init_active = gconf_value_get_bool (init_active_value);
    }

    dsme_active_value = gconf_entry_get_value (dsme_active_entry);

    if (NULL != dsme_active_value) {
        *dsme_active = gconf_value_get_bool (dsme_active_value);
    }

    alarm_event_value = gconf_entry_get_value (alarm_event_entry);

    if (NULL != alarm_event_value) {
        *alarm_event = gconf_value_get_int (alarm_event_value);
    }

    alarm_reboot_value = gconf_entry_get_value (alarm_reboot_entry);

    if (NULL != alarm_reboot_value) {
        *alarm_reboot = gconf_value_get_bool (alarm_reboot_value);
    }


cleanup:

    if (NULL != fullscreen_active_entry) {
        gconf_entry_unref (fullscreen_active_entry);
    }
    if (NULL != init_active_entry) {
        gconf_entry_unref (init_active_entry);
    }
    if (NULL != dsme_active_entry) {
        gconf_entry_unref (dsme_active_entry);
    }
    if (NULL != alarm_event_entry) {
        gconf_entry_unref (alarm_event_entry);
    }
    if (NULL != alarm_reboot_entry) {
        gconf_entry_unref (alarm_reboot_entry);
    }
    g_object_unref (client);
}


static void
_set_gconf_settings (gboolean fullscreen_active,
                     gboolean init_active,
                     gboolean dsme_active,
                     gint alarm_event,
                     gboolean alarm_reboot)
{
    GConfClient *client = NULL;
    gchar *tmp_string = NULL;


    client = gconf_client_get_default ();

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_fullscreen_active_key);
    gconf_client_set_bool (client,
                           tmp_string,
                           fullscreen_active,
                           NULL);
    g_free (tmp_string);

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_init_active_key);
    gconf_client_set_bool (client,
                           tmp_string,
                           init_active,
                           NULL);
    g_free (tmp_string);

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_dsme_active_key);
    gconf_client_set_bool (client,
                           tmp_string,
                           dsme_active,
                           NULL);
    g_free (tmp_string);

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_alarm_event_key);
    gconf_client_set_int (client,
                           tmp_string,
                           alarm_event,
                           NULL);
    g_free (tmp_string);

    tmp_string = g_strdup_printf ("%s/%s",
                                  gconf_skype_autolauncher_path,
                                  gconf_alarm_reboot_key);
    gconf_client_set_bool (client,
                           tmp_string,
                           alarm_reboot,
                           NULL);
    g_free (tmp_string);


/* cleanup: */

    g_object_unref (client);
}


static void
_activate_associated_widget_to_toggle_toggled (GtkToggleButton *toggle_button,
                                               GtkWidget *widget)
{
    g_assert (NULL != toggle_button);
    g_assert (NULL != widget);
    g_assert (GTK_IS_TOGGLE_BUTTON (toggle_button));
    g_assert (GTK_IS_WIDGET (widget));


    gtk_widget_set_sensitive (widget,
                              gtk_toggle_button_get_active (toggle_button));
}


static gboolean
_compare_alarm_events (alarm_event_t *first_event,
                       alarm_event_t *second_event)
{
    gboolean result = FALSE;

    g_assert (NULL != first_event);
    g_assert (NULL != second_event);


    if (first_event->alarm_time != second_event->alarm_time) {
        goto cleanup;
    }
    if (first_event->recurrence != second_event->recurrence) {
        goto cleanup;
    }
    if (first_event->recurrence_count != second_event->recurrence_count) {
        goto cleanup;
    }
    if (first_event->snooze != second_event->snooze) {
        goto cleanup;
    }
    if ((first_event->title != second_event->title) &&
        (NULL != first_event->title) &&
        (NULL != second_event->title) &&
        (strcmp (first_event->title,
                 second_event->title) != 0)) {
        goto cleanup;
    }
    if ((first_event->message != second_event->message) &&
        (NULL != first_event->message) &&
        (NULL != second_event->message) &&
        (strcmp (first_event->message,
                 second_event->message) != 0)) {
        goto cleanup;
    }
    if ((first_event->sound != second_event->sound) &&
        (NULL != first_event->sound) &&
        (NULL != second_event->sound) &&
        (strcmp (first_event->sound,
                 second_event->sound) != 0)) {
        goto cleanup;
    }
    if ((first_event->icon != second_event->icon) &&
        (NULL != first_event->icon) &&
        (NULL != second_event->icon) &&
        (strcmp (first_event->icon,
                 second_event->icon) != 0)) {
        goto cleanup;
    }
    if ((first_event->dbus_interface != second_event->dbus_interface) &&
        (NULL != first_event->dbus_interface) &&
        (NULL != second_event->dbus_interface) &&
        (strcmp (first_event->dbus_interface,
                 second_event->dbus_interface) != 0)) {
        goto cleanup;
    }
    if ((first_event->dbus_service != second_event->dbus_service) &&
        (NULL != first_event->dbus_service) &&
        (NULL != second_event->dbus_service) &&
        (strcmp (first_event->dbus_service,
                 second_event->dbus_service) != 0)) {
        goto cleanup;
    }
    if ((first_event->dbus_path != second_event->dbus_path) &&
        (NULL != first_event->dbus_path) &&
        (NULL != second_event->dbus_path) &&
        (strcmp (first_event->dbus_path,
                 second_event->dbus_path) != 0)) {
        goto cleanup;
    }
    if ((first_event->dbus_name != second_event->dbus_name) &&
        (NULL != first_event->dbus_name) &&
        (NULL != second_event->dbus_name) &&
        (strcmp (first_event->dbus_name,
                 second_event->dbus_name) != 0)) {
        goto cleanup;
    }
    if ((first_event->exec_name != second_event->exec_name) &&
        (NULL != first_event->exec_name) &&
        (NULL != second_event->exec_name) &&
        (strcmp (first_event->exec_name,
                 second_event->exec_name) != 0)) {
        goto cleanup;
    }
    if (first_event->flags != second_event->flags) {
        goto cleanup;
    }
    if (first_event->snoozed != second_event->snoozed) {
        goto cleanup;
    }

    result = TRUE;

cleanup:

    return result;
}


static void
_set_alarm_widgets (gint *alarm_event,
                    GtkWidget **alarm_check,
                    GtkWidget **alarm_time_editor)
{
    gboolean alarm_set = FALSE;
    alarm_event_t *event = NULL;

    g_assert (NULL != alarm_check);
    g_assert (NULL != alarm_time_editor);


    if (0 != *alarm_event) {
        event = alarm_event_get (*alarm_event);
        if (NULL != event) {
            alarm_event_t alarm_event_check;

            alarm_event_check = alarm_template;
            alarm_event_check.alarm_time = event->alarm_time;
            alarm_set = _compare_alarm_events (&alarm_event_check, event);
        }
    }

    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (*alarm_check), alarm_set);

    if (alarm_set) {
	struct tm *alarm_time;

	alarm_time = localtime(&event->alarm_time);

        hildon_time_editor_set_time (HILDON_TIME_EDITOR (*alarm_time_editor),
                                     alarm_time->tm_hour,
                                     alarm_time->tm_min,
                                     alarm_time->tm_sec);
    } else {
        *alarm_event = 0;
    }

    if (NULL != event) {
        alarm_event_free (event);
    }
}


static gint
_set_alarm (gint alarm_event,
            gboolean alarm_active,
            guint hours,
            guint minutes)
{
    gint result = 0;

    alarm_event_del (alarm_event);

    if (alarm_active) {
        alarm_event_t new_alarm;
	time_t current_time;
	struct tm *alarm_time;

        new_alarm = alarm_template;

	time (&current_time);
	alarm_time = localtime (&current_time);
	alarm_time->tm_hour = hours;
	alarm_time->tm_min = minutes;
	new_alarm.alarm_time = mktime (alarm_time);

        result = alarm_event_add (&new_alarm);
    }

    return result;
}


static gboolean
_restart_init (void)
{
    GError *error = NULL;
    gchar **argv = NULL;
    gchar *tmp = NULL;
    gint argc;
    gboolean result = FALSE;


    tmp = g_strdup_printf ("%s restart", init_script);

    g_shell_parse_argv (tmp, &argc, &argv, &error);

    if (error && (0 != error->code)) {
        goto cleanup;
    }

    if (error) {
        g_error_free (error);
    }

    g_spawn_async (NULL, argv, NULL,
                   G_SPAWN_STDOUT_TO_DEV_NULL |
                   G_SPAWN_STDERR_TO_DEV_NULL,
                   NULL, NULL,
                   NULL, &error);

    if (error && (0 != error->code)) {
        goto cleanup;
    }

    result = TRUE;


cleanup:

    g_free (tmp);
    g_strfreev (argv);
    if (error) {
        g_error_free (error);
    }

    return result;
}


static GtkDialog *
_skype_autolauncher_dialog_new (GtkWidget **fullscreen_check,
                                GtkWidget **init_check,
                                GtkWidget **dsme_check,
                                GtkWidget **alarm_check,
                                GtkWidget **reboot_check,
                                GtkWidget **alarm_time_editor)
{
    GtkWidget *dialog = NULL;
    GtkWidget *table = NULL;
    GtkWidget *label = NULL;
    GtkWidget *alignment = NULL;

    g_assert (NULL != fullscreen_check);
    g_assert (NULL != init_check);
    g_assert (NULL != dsme_check);
    g_assert (NULL != alarm_check);
    g_assert (NULL != reboot_check);
    g_assert (NULL != alarm_time_editor);


    dialog = gtk_dialog_new_with_buttons ("Automatic Skype Launcher Settings",
                                          NULL,
                                          GTK_DIALOG_DESTROY_WITH_PARENT |
                                          GTK_DIALOG_NO_SEPARATOR,
                                          GTK_STOCK_OK,
                                          GTK_RESPONSE_ACCEPT,
                                          GTK_STOCK_CANCEL,
                                          GTK_RESPONSE_REJECT,
                                          NULL);

    *fullscreen_check = gtk_check_button_new ();
    *init_check = gtk_check_button_new ();
    *dsme_check = gtk_check_button_new ();
    *alarm_check = gtk_check_button_new ();
    *reboot_check = gtk_check_button_new ();
    *alarm_time_editor = hildon_time_editor_new ();

    g_signal_connect (G_OBJECT (*init_check), "toggled",
                      G_CALLBACK (_activate_associated_widget_to_toggle_toggled),
                      *dsme_check);
    g_signal_connect (G_OBJECT (*alarm_check), "toggled",
                      G_CALLBACK (_activate_associated_widget_to_toggle_toggled),
                      *reboot_check);
    g_signal_connect (G_OBJECT (*alarm_check), "toggled",
                      G_CALLBACK (_activate_associated_widget_to_toggle_toggled),
                      *alarm_time_editor);

    table = gtk_table_new (6, 3, FALSE);
    gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), table);
    alignment = gtk_alignment_new (0, 0, 0, 0);
    gtk_container_add (GTK_CONTAINER (alignment), *init_check);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               alignment,
                               0, 1, 0, 1);
    label = gtk_label_new ("Start Skype automatically on startup?");
    gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               label,
                               1, 3, 0, 1);
    alignment = gtk_alignment_new (0, 0, 0, 0);
    gtk_container_add (GTK_CONTAINER (alignment), *dsme_check);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               alignment,
                               1, 2, 1, 2);
    label = gtk_label_new ("Re-start Skype on eventual crashes or when "
                           "closing?");
    gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               label,
                               2, 3, 1, 2);
    alignment = gtk_alignment_new (0, 0, 0, 0);
    gtk_container_add (GTK_CONTAINER (alignment), *fullscreen_check);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               alignment,
                               0, 1, 2, 3);
    label = gtk_label_new ("Try to set Skype in full screen mode automatically "
                           "just\nafter launching?");
    gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               label,
                               1, 3, 2, 3);
    alignment = gtk_alignment_new (0, 0, 0, 0);
    gtk_container_add (GTK_CONTAINER (alignment), *alarm_check);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               alignment,
                               0, 1, 3, 4);
    label = gtk_label_new ("Re-start Skype automatically every day at a given "
                           "time?");
    gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               label,
                               1, 3, 3, 4);
    alignment = gtk_alignment_new (0, 0, 0, 0);
    gtk_container_add (GTK_CONTAINER (alignment), *reboot_check);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               alignment,
                               1, 2, 4, 5);
    label = gtk_label_new ("Re-start the device instead of just Skype?");
    gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               label,
                               2, 3, 4, 5);
    alignment = gtk_alignment_new (0, 0, 0, 0);
    gtk_container_add (GTK_CONTAINER (alignment), *alarm_time_editor);
    gtk_table_attach_defaults (GTK_TABLE (table),
                               alignment,
                               1, 3, 5, 6);
    gtk_widget_show_all (dialog);

    return GTK_DIALOG (dialog);
}


static void
_skype_autolauncher_dialog_show ()
{
    GtkWidget *dialog = NULL;
    GtkWidget *fullscreen_check = NULL;
    GtkWidget *init_check = NULL;
    GtkWidget *dsme_check = NULL;
    GtkWidget *alarm_check = NULL;
    GtkWidget *reboot_check = NULL;
    GtkWidget *alarm_time_editor = NULL;
    gboolean fullscreen_active = TRUE;
    gboolean init_active = TRUE;
    gboolean dsme_active = TRUE;
    gboolean alarm_active = FALSE;
    gboolean alarm_reboot = FALSE;
    gint orig_alarm_event = 0;
    gint alarm_event = 0;
    guint hours;
    guint minutes;
    guint seconds;
    gboolean modified = FALSE;
    gboolean restart_init = FALSE;

    dialog = GTK_WIDGET (_skype_autolauncher_dialog_new (&fullscreen_check,
                                                         &init_check,
                                                         &dsme_check,
                                                         &alarm_check,
                                                         &reboot_check,
                                                         &alarm_time_editor));

    _get_gconf_settings (&fullscreen_active,
                         &init_active,
                         &dsme_active,
                         &orig_alarm_event,
                         &alarm_reboot);

    alarm_event = orig_alarm_event;

    _set_alarm_widgets (&alarm_event,
                        &alarm_check,
                        &alarm_time_editor);

    if (alarm_event != orig_alarm_event) {
        modified = TRUE;
    }

    alarm_active =
        gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (alarm_check));
    hildon_time_editor_get_time (HILDON_TIME_EDITOR (alarm_time_editor),
                                             &hours,
                                             &minutes,
                                             &seconds);

    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (reboot_check),
                                  alarm_reboot);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (fullscreen_check),
                                  fullscreen_active);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dsme_check),
                                  dsme_active);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (init_check),
                                  init_active);
    gtk_toggle_button_toggled (GTK_TOGGLE_BUTTON (init_check));
    gtk_toggle_button_toggled (GTK_TOGGLE_BUTTON (alarm_check));

    if (GTK_RESPONSE_ACCEPT == gtk_dialog_run (GTK_DIALOG (dialog))) {
        gboolean new_fullscreen_active =
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (fullscreen_check));
        gboolean new_init_active =
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (init_check));
        gboolean new_dsme_active =
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dsme_check));
        gboolean new_alarm_active =
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (alarm_check));
        gboolean new_alarm_reboot =
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (reboot_check));
        guint new_hours;
        guint new_minutes;
        guint new_seconds;
        hildon_time_editor_get_time (HILDON_TIME_EDITOR (alarm_time_editor),
                                     &new_hours,
                                     &new_minutes,
                                     &new_seconds);

        if ((fullscreen_active != new_fullscreen_active) ||
            (init_active != new_init_active) ||
            (new_init_active && (dsme_active != new_dsme_active))) {
            GtkWidget *label = NULL;
            GtkWidget *message_dialog = NULL;
            GtkWidget *restart_init_dialog =
                gtk_dialog_new_with_buttons ("Apply new settings?",
                                             dialog,
                                             GTK_DIALOG_DESTROY_WITH_PARENT |
                                             GTK_DIALOG_NO_SEPARATOR,
                                             GTK_STOCK_OK,
                                             GTK_RESPONSE_ACCEPT,
                                             GTK_STOCK_CANCEL,
                                             GTK_RESPONSE_REJECT,
                                             NULL);

            label = gtk_label_new ("Do you want to apply the changes now?");
            gtk_container_add (GTK_CONTAINER (GTK_DIALOG (restart_init_dialog)->vbox),
                               label);
            gtk_widget_show_all (restart_init_dialog);

            switch (gtk_dialog_run (GTK_DIALOG (restart_init_dialog))) {
            case GTK_RESPONSE_ACCEPT:
                restart_init = TRUE;
                break;
            default:
                message_dialog =
                    gtk_message_dialog_new (GTK_WINDOW (restart_init_dialog),
                                            GTK_DIALOG_DESTROY_WITH_PARENT,
                                            GTK_MESSAGE_INFO,
                                            GTK_BUTTONS_OK,
                                            "The changes will be applied "
                                            "on next reboot");

                gtk_dialog_run (GTK_DIALOG (message_dialog));
                gtk_widget_destroy (message_dialog);
                break;
            }
            modified = TRUE;
        }

        if ((alarm_active != new_alarm_active) ||
            (new_alarm_active &&
             ((alarm_reboot != new_alarm_reboot) ||
              (hours != new_hours) ||
              (minutes != new_minutes) ||
              (seconds != new_seconds)))) {
            modified = TRUE;
        }

        if (modified) {
            gint new_alarm_event = 0;

            new_alarm_event = _set_alarm (alarm_event,
                                          new_alarm_active,
                                          new_hours,
                                          new_minutes);

            _set_gconf_settings (new_fullscreen_active,
                                 new_init_active,
                                 new_dsme_active,
                                 new_alarm_event,
                                 new_alarm_reboot);

            if (restart_init) {
                _restart_init ();
            }
        }
    }

    gtk_widget_destroy (dialog);
}
