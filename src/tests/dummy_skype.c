/* gcc dummy_skype.c -o skype `pkg-config --cflags --libs gtk+-2.0` */

#include <gtk/gtk.h>

static void
_destroy( GtkWidget *widget,
          gpointer   data )
{
    gtk_main_quit ();
}

static GtkWidget *
_icon_new (void)
{
    GtkIconTheme *icon_theme;
    GdkPixbuf *icon;
    GtkWidget *icon_image;


    icon_theme = gtk_icon_theme_get_default ();
    icon = gtk_icon_theme_load_icon (icon_theme,
                                     "skype",
                                     64,
                                     0,
                                     NULL);

    if (NULL == icon) {
        icon = gtk_icon_theme_load_icon (icon_theme,
                                         "skype-autolauncher",
                                         64,
                                         0,
                                         NULL);
    }

    if (NULL == icon) {
        icon = gtk_icon_theme_load_icon (icon_theme,
                                         "qgn_list_gene_default_app",
                                         64,
                                         0,
                                         NULL);
    }

    icon_image = gtk_image_new_from_pixbuf (icon);
    g_object_unref (G_OBJECT (icon));

    return icon_image;
}

int main( int   argc,
          char *argv[] )
{
    GtkWidget *window;
    GtkWidget *icon;

    gtk_init (&argc, &argv);

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    g_signal_connect (window, "delete-event",
		      G_CALLBACK (_destroy), NULL);
    g_signal_connect (window, "destroy",
		      G_CALLBACK (_destroy), NULL);

    icon = _icon_new ();
    gtk_container_add (GTK_CONTAINER (window), icon);

    gtk_widget_show_all (window);

    gtk_main ();

    return 0;
}
